package model;

import java.util.Collections;
import java.util.HashMap; 
import java.util.Map;
import java.util.TreeMap;



public class Article implements Comparable<Article> {

    public Article(String title, String code){
    	m_title=title;
    	m_code=code; 
    }

    public String getTitle(){

        return m_title; 

    }

    public String getCode(){

        return m_code; 

    }

    public int numberOfAuthors(){

        return m_authors.size(); 

    }

    public void addAuthor(String name, String pos){
    	if(!(m_authors.keySet().contains(pos))){
    		m_authors.put(pos, name);
    	}
    }

    public String getAuthor(String pos){

        return m_authors.get(pos); 

    }

    public String toString(){

        return ""+m_code+" "+m_title;

    }

    public boolean equals(Article obj){

        if(m_code.equals(obj.getCode())){
        	return true;
        }
        return false;

    }
    
    public int compareTo(Article obj){
    	
    	return this.getCode().compareTo(obj.getCode());
    	
    }
    
    public void setIssue(Issue theIssue){
    	m_issue=theIssue;
    }
    
    public Issue getIssue(){
    	return m_issue;
    }
    
    public String allString(){
    	String tab ="\t";
    	StringBuffer allString= new StringBuffer();
    	allString.append("Code"+tab+m_code+"\n\n");
    	allString.append("Title"+tab+m_title+"\n\n");
    	allString.append("Authors"+tab);
    	
    	Map<String, String> map = new TreeMap<String, String>(m_authors);
    	for(Map.Entry<String, String> entry: map.entrySet()){
    		allString.append(entry.getKey()+" "+entry.getValue()+"\n"+tab);
    	}
    	
    	return allString.toString();
    }

    private Issue m_issue = new Issue();
    
    private String m_title; 

    private String m_code; 

    private Map<String, String> m_authors = new HashMap<String, String>();
   

}
