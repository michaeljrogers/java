package model;

import java.util.ArrayList;
import java.util.List;

public class Issue  implements Comparable <Issue>{
	
	public Issue(int volume, int number){
		m_volume=volume;
		m_number=number;
	
	}
	
	public Issue(){
		
	}
	
	//Returns the volume of the issue
	public int getVolume(){
		return m_volume;
	}
	
	//Sets the volume of the issue
	public void setVolume(int volume){
		m_volume=volume;
	}
	
	//Returns the number of the issue
	public int getNumber(){
		return m_number;
	}
	
	//Sets the number of the issue
	public void setNumber(int number){
		m_number=number;
	}
	
	//Returns the number of articles in the issue
	public int numberOfArticles(){
		return m_articles.size();
	
	}
	
	//Adds an article to the issue
	public void addArticle(Article article){
		article.setIssue(this);
		m_articles.add(article);
	}
	
	//returns the article located at specified index
	public Article getArticle(int index){
		if(index<numberOfArticles())
			return m_articles.get(index);
		return null;
		
	}
	
	//Returns the article located at the end of the issue
	public Article getLastArticle(){
		if(numberOfArticles()>=1)
			return m_articles.get(numberOfArticles()-1);
		return null;
	}
	//Returns the list of articles
	public List<Article> getArticles(){
		return m_articles;
	}
	
	
	//Returns a String with a list of articles with volume and number
	public String toString(){
		String toString="";
		toString+="Volume: "+m_volume+"     Number: "+m_number;
		/*
		for(Article i: m_articles){
			toString +=i.getTitle();
		}
		*/
		return toString;
	}
	
	//Returns if the obj state equals this object instance
	public boolean equals(Issue obj){
		if(obj.getNumber()== m_number && obj.getVolume()==m_volume)
			return true;
		return false;
	}
	
	public int compareTo(Issue obj){
	    	
	   if(m_number == obj.getNumber() && m_volume==obj.getVolume())
	    	return 0;
	   else if(obj.getVolume()==m_volume && m_number> obj.getNumber())
		   return -1;
	   else if(obj.getVolume()==m_volume && m_number< obj.getNumber())
		   return 1;
	   else if(m_volume>obj.getVolume())
		   return 1;
	   return -1;
		
	}
	
	private ArrayList<Article> m_articles = new ArrayList<Article>();
	private int m_number;
	private int m_volume;
	
	
}
