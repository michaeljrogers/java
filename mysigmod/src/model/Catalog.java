package model;

import java.util.ArrayList;
import java.util.Collections;


public class Catalog {
	
	//Constructor	
	public Catalog(){
			
	}
	
	//Add an issue object
	public void addIssue(Issue issue){
		m_issues.add(issue);
	}
	
	//return the number of issues maintained by the catalog
	public int numberOfIssues(){
		return m_issues.size();
	}
	
	//Return the issue stored at a position in the catalog
	public Issue getIssue(int index){
		if (index<numberOfIssues())
			return m_issues.get(index);
		return null;
	}
		
	//Return the issue stored at the end of the catalog
	public Issue getLastIssue(){
		if(numberOfIssues()>0)
			return m_issues.get((numberOfIssues()-1));
		return null;
	}
	
	//Search an issue by volume and number
	public Issue search(int volume, int number) {
		Issue dummy=new Issue(volume, number);
		Collections.sort(m_issues);
		int index=Collections.binarySearch(m_issues, dummy);
		if(index>=0)
			return m_issues.get(index);
		return null;
	}
	
	//Search an article by the article code
	public Article search(String code){
		Article dummy = new Article("",code);
		for(Issue i: m_issues){
			int index=Collections.binarySearch(i.getArticles(), dummy);
			if(index>=0)
				return i.getArticles().get(index);
		}
		return null;
	}
	
	//Return the string representing all issues stored in the catalog
	public String toString(){
		String toString="";
		for(Issue i: m_issues){
			toString +="Volume "+i.getVolume()+" Number "+i.getNumber()+"/n";
		}
		return toString;

	}
	private ArrayList<Issue> m_issues = new ArrayList<Issue>();
}
