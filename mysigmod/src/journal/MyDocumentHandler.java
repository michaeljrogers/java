package journal;

import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import java.util.Stack;
import model.*;

public class MyDocumentHandler extends DefaultHandler {
	private Catalog m_catalog;

	private Stack<StringBuffer> m_stack;
	private boolean m_isStackReadyForText;

	private String m_code = null;
	private Article m_article = null;
	private Issue m_issue = null;

	private final boolean LI_DEBUG = false;

	// -----

	public MyDocumentHandler() {
		m_stack = new Stack<StringBuffer>();
		m_catalog = new Catalog();
		m_isStackReadyForText = false;
	}

	public Catalog getCatalog() {
		return m_catalog;
	}

	// ----- callbacks: -----

	public void startElement(String uri, String localName, String qName,
			Attributes attribs) {
		m_isStackReadyForText = false;

		if ("".equals(localName)) {
			localName = qName;
		}
		if (localName.equals("issue")) {
			m_issue = new Issue();
		} else if (localName.equals("volume")) {
			m_stack.push(new StringBuffer());
			m_isStackReadyForText = true;
		} else if (localName.equals("number")) {
			m_stack.push(new StringBuffer());
			m_isStackReadyForText = true;
		} else if (localName.equals("article")) {

		} else if (localName.equals("title")) {
			m_stack.push(new StringBuffer());
			m_code = resolveAttrib(uri, "articleCode", attribs, "unknown");
			m_isStackReadyForText = true;
		}
		// if next element is simple, push StringBuffer
		// this makes the stack ready to accept character text
		else if (localName.equals("author")) {
			String tmp = resolveAttrib(uri, "AuthorPosition", attribs,
					"unknown");
			m_stack.push(new StringBuffer(tmp));
			m_stack.push(new StringBuffer());
			m_isStackReadyForText = true;
		}
		// if none of the above, it is an unexpected element
		else {
			// do nothing
		}
	}

	// -----

	public void endElement(String uri, String localName, String qName) {
		// recognized text is always content of an element
		// when the element closes, no more text should be expected
		m_isStackReadyForText = false;
		// pop stack and add to 'parent' element, which is next on the stack
		// important to pop stack first, then peek at top element!
		Object tmp = null;
		if (LI_DEBUG) {
			System.err.println("localName = " + localName);
			System.err.println("qName = " + qName);
		}
		if ("".equals(localName)) {
			localName = qName;
		}
		if (localName.equals("issue")) {
			m_catalog.addIssue(m_issue);

		} else if (localName.equals("article")) {
			m_issue.addArticle(m_article);

		} else if (localName.equals("volume")) {
			tmp = m_stack.pop();
			m_issue.setVolume(Integer.valueOf(tmp.toString()).intValue());
		} else if (localName.equals("number")) {
			tmp = m_stack.pop();
			m_issue.setNumber(Integer.valueOf(tmp.toString()).intValue());
		}
		// for simple elements, pop StringBuffer and convert to String
		else if (localName.equals("title")) {
			tmp = m_stack.pop();
			m_article = new Article(tmp.toString(), m_code);
		} else if (localName.equals("author")) {
			tmp = m_stack.pop();
			m_article.addAuthor(tmp.toString(),
					((StringBuffer) (m_stack.pop())).toString());
		}
		// if none of the above, it is an unexpected element
		// necessary to push popped element back!
		else {

		}
	}

	public void characters(char[] data, int start, int length) {
		// if stack is not ready, data is not content of recognized element
		if (m_isStackReadyForText == true) {
			m_stack.peek().append(data, start, length);
		} else {
			// read data which is not part of recognized element
		}
	}

	// -----

	private String resolveAttrib(String uri, String localName,
			Attributes attribs, String defaultValue) {

		String tmp = attribs.getValue(localName);
		return (tmp != null) ? (tmp) : (defaultValue);
	}
}
