package journal;

import java.io.*;

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;

import view.SystemView;
import model.*;

public class DataDriver {

	public static void main(String[] args) {
		String filename = "SigmodRecord.xml";
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			factory.setValidating(true);
			SAXParser saxParser = factory.newSAXParser();

			MyDocumentHandler saxUms = new MyDocumentHandler();
			saxParser.parse(new File(filename), saxUms);
			catalog = saxUms.getCatalog();

		} catch (Exception exc) {
			System.err.println("Exception: " + exc);
			exc.printStackTrace();
		}

		// create the SIGMOD journal system view with the data catalog
		SystemView view = new SystemView(catalog);
		// show the view
		view.setVisible(true);

	}

	static public Catalog catalog;
}
