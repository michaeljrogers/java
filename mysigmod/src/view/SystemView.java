package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Article;
import model.Catalog;
import model.Issue;

public class SystemView extends JFrame {
	private JTextField articleSearch = new JTextField("Enter Article Code", 15);
	private JTextField issueSearch = new JTextField("Enter Volume, Number", 15);
	private JButton searchBtn = new JButton("Search");
	private CatalogView catalogview;
	private Catalog catalog;

	/**
	 * Constructs a new SystemView object with a number of GUI objects
	 * 
	 * @param c
	 *            a Catalog data object
	 */
	public SystemView(Catalog c) {
		JPanel searchPanel = new JPanel();
		searchPanel.setLayout(new FlowLayout());
		searchPanel.add(new JLabel("Issue"));
		searchPanel.add(issueSearch);
		searchPanel.add(new JLabel("Article"));
		searchPanel.add(articleSearch);
		searchPanel.add(searchBtn);
		searchPanel.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		catalog = c;
		catalogview = new CatalogView(c);

		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(catalogview, BorderLayout.CENTER);
		getContentPane().add(searchPanel, BorderLayout.SOUTH);

		addSearchBtnListener(new SearchBtnListener());
		addIssueTextListener(new IssueTextListener());
		addArticleTextListener(new ArticleTextListener());
		addTextFieldListener(new TxtFieldFocusListener());

		setTitle("Online Journal Catalog");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(800, 650);
		setVisible(true);
	}

	/**
	 * Sets the text displayed in the Issue search text box
	 * 
	 * @param str
	 *            a string to display in the Issue search text box
	 */
	public void setIssueSearchTxt(String str) {
		issueSearch.setText(str);
	}

	/**
	 * Returns the string value of the Issue search text box
	 * 
	 * @return string value of the Issue search text box
	 */
	public String getIssueSearchTxt() {
		return issueSearch.getText();
	}

	/**
	 * Sets the text displayed in the Article search text box
	 * 
	 * @param str
	 *            a string to display in the Article search text box
	 */
	public void setArticleSearchTxt(String str) {
		articleSearch.setText(str);
	}

	/**
	 * Returns the string value of the Article search text box
	 * 
	 * @return string value of the Article search text box
	 */
	public String getArticleSearchTxt() {
		return articleSearch.getText();
	}

	/**
	 * Adds the listener object to the search button in the SystemView
	 * 
	 * @param listener
	 *            an action listener object
	 */
	public void addSearchBtnListener(ActionListener listener) {
		// add code here
		searchBtn.addActionListener(listener);
	}

	/**
	 * Adds the focus listener object to the both search text boxes in
	 * SystemView
	 * 
	 * @param listener
	 *            an focus listener object
	 */
	public void addTextFieldListener(FocusListener listener) {
		articleSearch.addFocusListener(listener);
		issueSearch.addFocusListener(listener);
	}

	/**
	 * Call appropriate method to set the Article displayed in the catalog tree
	 * 
	 * @param article
	 */
	public void select(Article article) {
		catalogview.dataArea.selectPath(catalogview.dataArea.select(article));
		

	}

	/**
	 * Call appropriate method to set the Issue displayed in the catalog tree
	 * 
	 * @param issue
	 */
	public void select(Issue issue) {
		catalogview.dataArea.selectPath(catalogview.dataArea.select(issue));
	}

	/**
	 * Set the value of the TextArea of the CatalogView
	 * 
	 * @param string
	 */
	public void setTextArea(String string) {
		// add code here
		catalogview.setTextArea(string);
	}

	/**
	 * Adds the listener object to the issue search text box in the SystemView
	 * 
	 * @param listener
	 *            an action listener object
	 */
	public void addIssueTextListener(ActionListener listener) {
		issueSearch.addActionListener(listener);
	}

	/**
	 * Adds the listener object to the article search text box in the SystemView
	 * 
	 * @param listener
	 *            an action listener object
	 */
	public void addArticleTextListener(ActionListener listener) {
		articleSearch.addActionListener(listener);
	}

	public void findIssue(String str) {
		int volume = 0, number = 0;
		str = str.replaceAll(" ", "");
		String[] temp = str.split(",");
		if (temp.length == 2) {
			volume = Integer.parseInt(temp[0]);
			number = Integer.parseInt(temp[1]);

			Issue issue = catalog.search(volume, number);
			if (issue != null)
				select(issue);
			else
				setTextArea("No Issue mathing your search was found.");
		} else
			setTextArea("Please enter a valid input of Volume, Number e.g. 20, 2");
	}

	public void findArticle(String str) {
		Article article = catalog.search(str);
		System.out.println("\narticle: " + article.getCode());
		if (article != null)
			select(article);
		else
			setTextArea("No Article mathing your search was found.");

	}

	class SearchBtnListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String strArticle = getArticleSearchTxt();
			String strIssue = getIssueSearchTxt();

			if (strArticle.length() > 0)
				findArticle(strArticle);
			else if (strIssue.length() > 0)
				findIssue(strIssue);
			else
				setTextArea("Please enter a value for either the Issue or Article to search.");
		}
	}

	class IssueTextListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			findIssue(e.getActionCommand());
		}
	}

	class ArticleTextListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			findArticle(e.getActionCommand());
		}
	}

	class TxtFieldFocusListener implements FocusListener {
		@Override
		public void focusGained(FocusEvent e) {
			setArticleSearchTxt("");
			setIssueSearchTxt("");
		}

		@Override
		public void focusLost(FocusEvent arg0) {
		}

	}

}
