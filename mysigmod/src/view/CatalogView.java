package view;

import javax.swing.*;

import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.*;
import java.util.Enumeration;

import model.*;

//you also need to import Catalog, Article, and Issue if you put them in another package

public class CatalogView extends JPanel {

	final static String NEWLINE = "\n";
	final static String TAB = "\t";

	public CatalogView(Catalog catalog) {

		super(new BorderLayout());
		System.out.println("In the constructor");
		dataArea = new DataArea(catalog);

		add(dataArea, BorderLayout.CENTER);

		articlePanel = new JPanel();
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setEditable(false);

		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		scrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		scrollPane.setPreferredSize(new Dimension(600, 200));

		articlePanel.add(scrollPane, BorderLayout.CENTER);

		add(articlePanel, BorderLayout.SOUTH);

		setPreferredSize(new Dimension(650, 650));
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
	}

	void displayArticle(String articleinfo) {

		textArea.setWrapStyleWord(true);
		textArea.setText(NEWLINE + articleinfo + NEWLINE);

	}

	public void setTextArea(String string) {
		textArea.setText(string);

	}

	class DataArea extends JScrollPane implements TreeExpansionListener,
			TreeSelectionListener {

		private Dimension minSize = new Dimension(100, 100);
		private JTree tree;

		public DataArea(Catalog catalog) {
			TreeNode rootNode = createNodes(catalog);
			tree = new JTree(rootNode);
			tree.addTreeExpansionListener(this);
			tree.addTreeSelectionListener(this);
			setViewportView(tree);
		}

		private TreeNode createNodes(Catalog catalog) {

			DefaultMutableTreeNode root;
			DefaultMutableTreeNode parent;
			DefaultMutableTreeNode child;

			root = new DefaultMutableTreeNode("SIGMOD RECORD");
			
			System.out.println("I made it in!  catalog.numberOfIssues() " +  catalog.numberOfIssues());
			for (int i = 0; i < catalog.numberOfIssues(); i++) {

				Issue issue = catalog.getIssue(i);
				
				parent = new DefaultMutableTreeNode("Volume "+new
				Integer(issue.getVolume()).toString()+" Number "+new
				Integer(issue.getNumber()).toString());
				
				parent = new DefaultMutableTreeNode(issue);

				root.add(parent);

				for (int i1 = 0; i1 < issue.numberOfArticles(); i1++) {

					Article article = issue.getArticle(i1);
					child = new DefaultMutableTreeNode(article);
					parent.add(child);
				}
			}

			return root;
		}

		public Dimension getMinimumSize() {

			return minSize;

		}

		public Dimension getPreferredSize() {

			return minSize;

		}

		// Required by TreeExpansionListener interface.
		public void treeExpanded(TreeExpansionEvent e) {
		}

		// Required by TreeExpansionListener interface.
		public void treeCollapsed(TreeExpansionEvent e) {
		}

		// Required by TreeSelectionLisener interface.
		public void valueChanged(TreeSelectionEvent e) {

			DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree
					.getLastSelectedPathComponent();

			if (node == null)
				return;

			if (node.isLeaf()) {
				Article article = (Article) node.getUserObject();
				// allString is a method defined in Article including article's
				// full details
				displayArticle(article.allString());
			} else {
				displayArticle("");
			}
		}

		public TreePath select(Article article) {

			// For my Article class, I have a method there in order to get which
			// issue the article locates
			TreePath path = select(article.getIssue());

			DefaultMutableTreeNode nodes[] = new DefaultMutableTreeNode[3];
			nodes[0] = (DefaultMutableTreeNode) path.getPathComponent(0);
			nodes[1] = (DefaultMutableTreeNode) path.getPathComponent(1);

			Enumeration<DefaultMutableTreeNode> children = nodes[1].children();
			while (children.hasMoreElements()) {

				DefaultMutableTreeNode child = children.nextElement();
				if (article.equals(child.getUserObject())) {
					nodes[2] = child;
					break;
				}
			}

			path = new TreePath(nodes);
			selectPath(path);
			return path;
		}

		public void selectPath(TreePath path) {
			// tree is an instance variable referring to the JTree object
			tree.clearSelection();

			TreeSelectionModel model = tree.getSelectionModel();
			tree.setExpandsSelectedPaths(true);

			tree.expandPath(path);
			tree.scrollPathToVisible(path);
			tree.setSelectionPath(path);

			model.setSelectionPath(path);

		}

		public TreePath select(Issue issue) {
			TreePath path = null;
			DefaultMutableTreeNode nodes[] = new DefaultMutableTreeNode[2];

			tree.clearSelection();

			nodes[0] = (DefaultMutableTreeNode) tree.getModel().getRoot();
			Enumeration<DefaultMutableTreeNode> children = nodes[0].children();

			while (children.hasMoreElements()) {
				DefaultMutableTreeNode child = children.nextElement();
				if (issue.equals(child.getUserObject())) {
					nodes[1] = child;
					break;
				}
			}

			path = new TreePath(nodes);
			selectPath(path);
			return path;

		}
	}// end of class DataArea

	protected DataArea dataArea;
	private JTextArea textArea;
	private JPanel articlePanel;
}