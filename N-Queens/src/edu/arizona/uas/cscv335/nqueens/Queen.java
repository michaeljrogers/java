//A couple of changes were made to make this class work for any number of queens
//Firstly, Queen now takes an extra int argument called numQueens
//Secondly, any reference to the magic number 8 has been replaced with the size 
//variable, which holds the number of Queens

package edu.arizona.uas.cscv335.nqueens;

public class Queen{
	
	static final public boolean DEBUG = true; 
	
	public Queen(int c, Queen n, int numQueens){
		row = 1; 
		column = c; 
		neighbor = n; 
		size = numQueens;
	}
	
	public int getColumn(){ return column; }
	
	public int getRow(){ return row; }
	
	public Queen getNeighbor(){return neighbor; }
	


	
	public boolean findSolution(){
		if (neighbor == null) {
			return true;
		}
		//moving down and up to find a position to sit peacefully with left neighbors
		while (neighbor.canAttack(row, column)){
			if (!next()){
				if (DEBUG)
					System.out.println("Queen " + column +" failed to find a solution!"); 
				return false; 
			}
		}
		return true; 
	}
	
	public boolean next(){
		if (row < size){
			row = row + 1; 
			return findSolution(); 
		}
		
		//cannot go further, i.e. row == N
		if (neighbor== null || !neighbor.next()){
			if (DEBUG){
				System.out.println("Queen " + column +"'s neighbor failed to find a solution!");
			}
		    
			return false;
		}
		else{
			row = 1;
			return(findSolution());
		}
	}
	
	public boolean canAttack(int row, int column){
		
		if (row == this.row) return true; 
		int cd = this.column - column; 
		if ((this.row + cd == row) ||(this.row - cd == row)){
			return true;
		}
		
		if (neighbor == null) return false; 
		else
			return neighbor.canAttack(row, column);
	}
	
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		buffer.append("Queen " + column + " at " + row +"\n");
		return buffer.toString();
	}
	
	public void print(){
		if (neighbor != null){
			neighbor.print();
		}
		System.out.println(this.toString());
		
	}
	
	private int row; 
	private int column;
	private Queen neighbor; 
	private int size;
	
	static public void main(String args[]){
		int size=8;
		Queen lastQueen = null;
		for (int i = 1; i <= size; i ++){
			Queen queen = new Queen(i, lastQueen, size);

			if (queen.findSolution())
				lastQueen = queen;
			else{
				System.out.println("The " + size +"-Queens problem doesn't have a solution!\n" +"\t (Queen " + queen.getColumn() +")"); 
				return;
			}
		}
		//print the positions of all queens on board
		lastQueen.print(); 	
	}
}
