package edu.arizona.uas.cscv335.nqueens;
public class Queens{
	
	//instance variables
	private int numQueens; //Number of Queens in the puzzle problem
	private Queen lastQueen = null; //Pointer for last queen. initiates at null
	
	
	//Constructor, takes n, which is board size ( n * n) and number of Queens
	public Queens(int n){ 
		numQueens=n;
	}
	
	
	//finds the first solution for our n-queens problem
	public String findFirstSolution(){
		for (int i =1; i <= numQueens; i++){
			Queen queen = new Queen(i, lastQueen, numQueens);
			
			if (queen.findSolution())
				lastQueen = queen;
			else{
				System.out.println("The " +numQueens+"-Queens problem doesn't have a solution!\n"+"\t (Queen " +queen.getColumn() +")");
				return textQueenSolution(queen, false);//return for no solutions
			}
		}
		return textQueenSolution(lastQueen, true); //return for first solution
	}
	
	
	//finds the next solution, and determines if one was found. 
	public String findNext(){
		lastQueen.next(); //gets the last queen into motion again, engaging the recursive cycle.
		if(lastQueen.findSolution()) //now we check whether or not a solution was found in the cycle we just started. 
			return textQueenSolution(lastQueen, true); //if yes, we return the text solution.
		return textQueenSolution(lastQueen, false);//return for no more solutions
		
	}
	
	
	//helper methods
	private String textQueenSolution(Queen q, boolean found){
		if (found){
			StringBuffer buf = new StringBuffer();
			if (q.getNeighbor()== null){
				buf.append("[" + q.getRow());
			}
			else{
				buf.append(textQueenSolution(q.getNeighbor(), found));
				buf.append(", " + q.getRow());
			}
			if (q.getColumn() == numQueens)
					buf.append("]");
			return buf.toString();
		}
		else{
			return "";
		}
	}
	
	
	static public void main (String[] args){
		Queens problem = new Queens(4);
		System.out.println(problem.findFirstSolution()); //[2, 4, 1, 3] if numQueens=4
		System.out.println(problem.findNext()); //[3, 1, 4, 2] if numQueens =4
	}
}
