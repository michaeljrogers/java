
public class Card {
       
	public Card (int suitValue, int rankValue) { 
		this.suitValue = suitValue; 
		this.rankValue = rankValue;
		faceUp = true; 
	}

	// operations on a playing card
	public boolean faceUp  ()           { return faceUp; }
	public int    rank      ()           { return rankValue; }
	public int   suit      ()           { return suitValue; }
	public void    setFaceUp (boolean up) { faceUp = up; }
	public void    flip      ()           { setFaceUp( !faceUp);}
	public int   color     ()           { 
		if ((suit() == DIAMOND) || (suit() == HEART)) 
			return RED;
		return BLACK;
	}
	// private data values
	private int suitValue;
	private int rankValue;
    private boolean faceUp;
    
    static final int SPADE = 0; 
    static final int DIAMOND = 1; 
    static final int CLUB = 2; 
    static final int HEART = 3; 
    
    static final int BLACK = 0; 
    static final int RED = 1; 
    
 


}

	