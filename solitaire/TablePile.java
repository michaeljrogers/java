import java.util.ListIterator;

class TablePile extends CardPile {

	TablePile (int x, int y, int c) {
			// initialize the parent class
		super(x, y);
			// then initialize our pile of cards
		for (int i = 0; i < c; i++) {
			addCard(Game.deckPile.pop());
			}
			// flip topmost card face up
		top().flip();
		}

	public boolean canTake (Card aCard) {
		if (empty())
			return aCard.rank() == 12;
		Card topCard = top();
		return (aCard.color() != topCard.color()) &&
			(aCard.rank() == topCard.rank() - 1);
		}

	public boolean includes (int tx, int ty) {
			// don't test bottom of card
		return x <= tx && tx <= x + CardView.Width &&
			y <= ty;
		}

	public void select (int tx, int ty) {
		if (empty())
			return;

			// if face down, then flip
		Card topCard = top();
		if (! topCard.faceUp()) {
			topCard.flip();
			return;
			}

			// else see if any suit pile can take card
		topCard = pop();
		for (int i = 0; i < 4; i++)
			if (Game.suitPile[i].canTake(topCard)) {
				Game.suitPile[i].addCard(topCard);
				return;
				}
			// else see if any other table pile can take card
		for (int i = 0; i < 7; i++)
			if (Game.tableau[i].canTake(topCard)) {
				Game.tableau[i].addCard(topCard);
				return;
				}
			// else put it back on our pile
		addCard(topCard);
		}


	public void display (CardView cv) {
		int hs = CardView.Height / 2; //half size
		int ty = y; 
		for (ListIterator<Card> i = pile.listIterator(); i.hasNext(); ){
			Card card = i.next();
			cv.display(card, x, ty);
			ty += hs; 
		    }
		    	
		}

}

