import java.awt.Color;
import java.awt.Graphics;


class WinFormsCardView extends CardView {

	public WinFormsCardView (Graphics aGraphicsOject){
		g = aGraphicsOject;
	}
	@Override
	public void display(Card card, int x, int y) {
		if (card == null){
			g.clearRect(x, y, Width, Height);
			g.setColor(Color.black);
			g.drawRect(x, y, Width, Height);
		}
		else{
			printCard(card, x, y);
		}

	}
	private void printCard(Card card, int x, int y){
		String names[] = {"A", "2", "3", "4", "5", "6",
				"7", "8", "9", "10", "J", "Q", "K"};
		
				// clear rectangle, draw border
			g.clearRect(x, y, Width, Height);
			g.setColor(Color.black);
			g.drawRect(x, y, Width, Height);
			
				// draw body of card
			if (card.faceUp()) {
				if (card.color() == Card.RED)
					g.setColor(Color.red);
				else
					g.setColor(Color.blue);
				g.drawString(names[card.rank()], x+3, y+15);
				if (card.suit() == Card.HEART) {
					g.drawLine(x+25, y+30, x+35, y+20);
					g.drawLine(x+35, y+20, x+45, y+30);
					g.drawLine(x+45, y+30, x+25, y+60);
					g.drawLine(x+25, y+60, x+5, y+30);
					g.drawLine(x+5, y+30, x+15, y+20);
					g.drawLine(x+15, y+20, x+25, y+30);
					}
				else if (card.suit() == Card.SPADE) {
					g.drawLine(x+25, y+20, x+40, y+50);
					g.drawLine(x+40, y+50, x+10, y+50);
					g.drawLine(x+10, y+50, x+25, y+20);
					g.drawLine(x+23, y+45, x+20, y+60);
					g.drawLine(x+20, y+60, x+30, y+60);
					g.drawLine(x+30, y+60, x+27, y+45); 
					}
				else if (card.suit() == Card.DIAMOND) {
					g.drawLine(x+25, y+20, x+40, y+40);
					g.drawLine(x+40, y+40, x+25, y+60);
					g.drawLine(x+25, y+60, x+10, y+40);
					g.drawLine(x+10, y+40, x+25, y+20);
					}
				else if (card.suit() == Card.CLUB) {
					g.drawOval(x+20, y+25, 10, 10);
					g.drawOval(x+25, y+35, 10, 10);
					g.drawOval(x+15, y+35, 10, 10);
					g.drawLine(x+23, y+45, x+20, y+55);
					g.drawLine(x+20, y+55, x+30, y+55);
					g.drawLine(x+30, y+55, x+27, y+45); 
					}
				}
			else { // face down
				g.setColor(Color.yellow);
				g.drawLine(x+15, y+5, x+15, y+65);
				g.drawLine(x+35, y+5, x+35, y+65);
				g.drawLine(x+5, y+20, x+45, y+20);
				g.drawLine(x+5, y+35, x+45, y+35);
				g.drawLine(x+5, y+50, x+45, y+50);
				}
			
	}
	private Graphics g; 

}
