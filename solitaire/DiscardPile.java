
class DiscardPile extends CardPile {
	
	DiscardPile (int x, int y) { super (x, y); }

	public void addCard (Card aCard) {
		if (! aCard.faceUp())
			aCard.flip();
		super.addCard(aCard);
		}

	public void select (int tx, int ty) {
		if (empty())
			return;
		Card topCard = pop();
		for (int i = 0; i < 4; i++)
			if (Game.suitPile[i].canTake(topCard)) {
				Game.suitPile[i].addCard(topCard);
				return;
				}
		for (int i = 0; i < 7; i++)
			if (Game.tableau[i].canTake(topCard)) {
				Game.tableau[i].addCard(topCard);
				return;
				}
		// nobody can use it, put it back on our list
		addCard(topCard);
		}
}
