import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;


public class Solitaire extends JFrame {
	public Solitaire(){
		super("Solitaire Game");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(500, 500));
		addMouseListener(new MouseKeepter());
	}
	
	private static void createAndShowGUI(){
		JFrame game = new Solitaire();
		game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//display the window
		game.pack();
		game.setVisible(true);
	}
	
	public static void main(String[] args){
		javax.swing.SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				createAndShowGUI();
			}
		});
	}
	public void paint(Graphics g){
		super.paint(g);
		Game.paint(new WinFormsCardView(g));
	}
	private boolean mouseDown(int x, int y){
		Game.mouseDown(x, y);
		repaint();
		return true;
	}
	private class MouseKeepter extends MouseAdapter{
		public void mousePressed (MouseEvent e){
			mouseDown(e.getX(), e.getY());
		}
	}

}
