
import java.util.Stack;

class CardPile {

	CardPile (int xl, int yl) {
		x = xl;
		y = yl;
		pile = new Stack<Card>();
		}

		// access to cards are not overridden

	public Card top() { return pile.peek(); }

	public boolean empty() { return pile.size() == 0; }

	public Card pop() { return pile.pop();}

		// the following are sometimes overridden

	public boolean includes (int tx, int ty) {
		return x <= tx && tx <= x + CardView.Width &&
			y <= ty && ty <= y + CardView.Height;
		}
	
	
	public void select (int tx, int ty) {
		// do nothing
		}
	

	public void addCard (Card aCard) {
		pile.push(aCard);
		}

	public void display (CardView cv) {
		if (empty())
			cv.display(null, x, y);
		else
			cv.display(pile.peek(), x, y);
		}

	public boolean canTake (Card aCard) {
		return false; 
		}

		// coordinates of the card pile
	protected int x;
	protected int y;
	protected Stack<Card> pile;
}

